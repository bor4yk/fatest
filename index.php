<?php
require_once('config.php');

$path = explode('/', parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
$controllerName = (!empty($path[1]) ? $path[1] : 'Index') . 'Controller';
$actionName = 'action' . (!empty($path[2]) ? ucfirst($path[2]) : 'Index');

require_once(ROOTDIR.'/Classes/checkUser.php');

if (!$user &&($controllerName != 'authController')) {
    header("Location: /auth");
}

if (file_exists(__DIR__.'/controllers/'.$controllerName.'.php')) {
    require_once(__DIR__ . '/controllers/' . $controllerName . '.php');
}
else
    $error = "404";

if (isset($error))
{
    die($error." :C");
}
?>