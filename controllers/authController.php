<?php
require_once(ROOTDIR."/Classes/db.php");
function actionIndex(){

}
function actionLogon($pdo){
    if (!isset($_POST['login']) || !isset($_POST['password'])) {
        header("Location: /auth");
        die();
    }
    $login = sanitizeString(trim($_POST['login']));
    $password = sanitizeString(trim($_POST['password']));
    /*if ($_POST['remember'] == "on")
        $remember = true;*/
    $stmt = $pdo->query("SELECT * FROM `users` WHERE `User_id` ='$login' AND `pwd`='$password'");
    $user = $stmt->fetch();
    if (!$user)
    {
        header("Location: /auth?err=1&login=".$login);
        die();
    }
    else
    {
        setcookie("i", $user['User_id'], time() + 2592000 ,"/");
        setcookie("h", $user['Pwd'], time() + 2592000 ,"/");

        header("Location: /");
        die();
    }
}

function actionLogout(){
    //mysqli_query($link, "UPDATE users SET last_login_time='".time()."' WHERE id='".$_COOKIE['i']."'");

    setcookie("i", "", 0, "/");
    setcookie("h", "", 0, "/");

    header("Location: /");
}

if ($user){
    header("Location: /");
    die();
}

switch ( $actionName){
    case 'actionLogon':
        actionLogon($pdo);
        break;
    case 'actionLogout':
        actionLogout();
    default:
        actionIndex();
}

require_once(VIEWDIR.'twig.php');
$attr = array(
    'template' => 'auth',
    'title' => 'Вход',
    'user' => $user
);
Template::init($attr);
?>