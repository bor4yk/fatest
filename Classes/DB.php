<?php
$dsn = "mysql:host=".MYSQL_SERVER.";dbname=".MYSQL_DB;
$opt = array(
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
);

$pdo = new PDO($dsn, MYSQL_USER, MYSQL_PASSWORD, $opt);

function sanitizeString($str)
{
    $str = strip_tags($str);
    $str = htmlspecialchars($str);
    $str = stripslashes($str);
    return $str;
}
?>