<?php

class Template {
    private static $instance = null;
    private $_attr = array();

    public function __construct($attr) {
        $this->_attr = array_merge($this->_attr, $attr);

        try {
            // Подгружаем и активируем авто-загрузчик Twig-а
            require_once 'twig/lib/Twig/Autoloader.php';
            Twig_Autoloader::register();

            // Указываем где хранятся шаблоны
            $loader = new Twig_Loader_Filesystem(VIEWDIR.'templates');

            // Инициализируем Twig
            $twig = new Twig_Environment($loader, array(
                'cache' => dirname(__file__).'/twig/compilation_cache',
                'debug' => false, // только для разработки
                'auto_reload' => true // включаем кэш
            ));


            // Подгружаем шаблон
            $template = $twig->loadTemplate($this->_attr['template'] . '.twig');

            // Передаём в шаблон переменные и значения
            // Выводим сформированное содержание
            echo $template->render($this->_attr);
        } catch (Exception $e) {
            throw new Exception('Ошибка: ' . $e->getMessage());
        }
    }

    public static function init($attr = array()) {
            self::$instance = new Template($attr);
        return self::$instance;
    }
}